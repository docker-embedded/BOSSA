# bossa
Docker image of BOSSA running on Debian

## Images
This Docker image is intended to run on both Linux x86_64 systems as well as a Raspberry Pi. The x86_64 image is built using the Gitlab provided runner while the Raspberry Pi version is built and tested directly on a Raspberry Pi 4 (versus cross compiled). THe following is a list of images and tags that are built off of this repository's Dockerfile:
1. [bossa:latest](registry.gitlab.com/docker41/bossa:latest) - Standard image used on Linux x86_64
2. [bossa:rpi](registry.gitlab.com/docker41/bossa:rpi) - Raspberry Pi image used on Raspberry Pi devices

## Toolsets
The intent for this image is not only to program target Atmel SAM devices but also run minimal hardware in the loop testing on them. As a result, other applications (in addition to BOSSA) have been installed with this image. 

## Additional Resources
A companion Docker image that enables one to compile Atmel source code can be found [here](https://gitlab.com/docker41/gcc-arm-none-eabi).