# Pull Debian image
FROM debian:latest

# Install packages
RUN apt update
RUN apt-get install -y --no-install-recommends \
		ca-certificates \
		git \
		libwxgtk3.0-dev \
		libreadline-dev \
		build-essential

# Clone BOSSA project, install, and verify
WORKDIR /opt
RUN git clone https://github.com/shumatech/BOSSA.git
WORKDIR BOSSA
RUN make install
WORKDIR /

# Set environment
ENV PATH="/opt/BOSSA/bin:${PATH}"